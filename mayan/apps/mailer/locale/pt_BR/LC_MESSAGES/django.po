# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Rogerio Falcone <rogerio@falconeit.com.br>, 2015
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-05-15 19:02-0400\n"
"PO-Revision-Date: 2015-05-13 17:29+0000\n"
"Last-Translator: Rogerio Falcone <rogerio@falconeit.com.br>\n"
"Language-Team: Portuguese (Brazil) (http://www.transifex.com/projects/p/mayan-edms/language/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: forms.py:23
msgid "Email address"
msgstr "Endereço de E-mail"

#: forms.py:24
msgid "Subject"
msgstr "Assunto"

#: forms.py:25
msgid "Body"
msgstr "Corpo"

#: links.py:7
msgid "Email link"
msgstr "Email de ligação"

#: links.py:8
msgid "Email document"
msgstr "Email documento"

#: permissions.py:7
msgid "Mailing"
msgstr "Mailing"

#: permissions.py:9
msgid "Send document link via email"
msgstr "Enviar link do documento por e-mail"

#: permissions.py:10
msgid "Send document via email"
msgstr "Enviar documento por  e-mail"

#: settings.py:11
msgid "Link for document: {{ document }}"
msgstr "Link para o documento: {{ document }}"

#: settings.py:11
msgid "Template for the document link email form subject line."
msgstr "Template para a linha de formulário electrónico Assunto link do documento."

#: settings.py:12
msgid ""
"To access this document click on the following link: <a href=\"{{ link }}\">{{ link }}</a><br /><br />\n"
"\n"
"--------<br />\n"
"This email has been sent from Mayan EDMS (http://www.mayan-edms.com)"
msgstr ""

#: settings.py:12
msgid "Template for the document link email form body line."
msgstr "Modelo para o documento, linha de corpo formulário de e-mail."

#: settings.py:13
msgid "Document: {{ document }}"
msgstr "Documento: {{ document }}"

#: settings.py:13
msgid "Template for the document email form subject line."
msgstr "Modelo para o documento, linha de assuntos e-mail."

#: settings.py:14
msgid ""
"Attached to this email is the document: {{ document }}<br /><br />\n"
"\n"
"--------<br />\n"
"This email has been sent from Mayan EDMS (http://www.mayan-edms.com)"
msgstr ""

#: settings.py:14
msgid "Template for the document email form body line."
msgstr "Modelo para o documento, linha de corpo formulário de e-mail."

#: views.py:41
msgid "Must provide at least one document."
msgstr "Deve fornecer, pelo menos, um documento."

#: views.py:70
msgid "Successfully queued for delivery via email."
msgstr "Sucesso da Fila para a entrega via e-mail."

#: views.py:78
msgid "Send"
msgstr "Enviar"

#: views.py:84
#, python-format
msgid "Email document: %s"
msgstr "E-mail de documentos: %s"

#: views.py:86
#, python-format
msgid "Email link for document: %s"
msgstr "link de E-mail  para  documento: %s "

#: views.py:89
#, python-format
msgid "Email documents: %s"
msgstr "E-mail de documentos: %s"

#: views.py:91
#, python-format
msgid "Email links for documents: %s"
msgstr "link de E-mail  para  documento: %s "
